package com.schopfen.mpksaproject.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.schopfen.mpksaproject.HelperClasses.BaseClass;
import com.schopfen.mpksaproject.HelperClasses.HomeGridItem;
import com.schopfen.mpksaproject.HelperClasses.ItemClickListener;
import com.schopfen.mpksaproject.HelperClasses.NotificationData;
import com.schopfen.mpksaproject.R;

import java.util.ArrayList;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {

    ArrayList<String> alName;
    ArrayList<NotificationData> alImage;
    Context context;
    BaseClass baseClass;



    public NotificationsAdapter(Context context, ArrayList<NotificationData> alName) {
        super();
        this.context = context;
        this.alImage = alName;
        baseClass = new BaseClass(context);


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.notification_item, viewGroup, false);
        final ViewHolder viewHolder = new ViewHolder(v);

//        v.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listener.onItemClick(v, viewHolder.getPosition());
//            }
//        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {

//
//        if (sharedpreferences.getString("lan", "").equals("english")) {
//
//            viewHolder.tvSpecies.setText(alImage.get(i).getProductModelEn());
//            application.setTypefaceTextView(viewHolder.tvSpecies);
//
//
//        } else if (sharedpreferences.getString("lan", "").equals("arabic")) {
//            viewHolder.tvSpecies.setText(alImage.get(i).getProductModelAr());
//            application.setTypefaceTextViewArabic(viewHolder.tvSpecies);
//
//        } else {
//            viewHolder.tvSpecies.setText(alImage.get(i).getProductModelEn());
//            application.setTypefaceTextView(viewHolder.tvSpecies);
////        }


//        viewHolder.avLoadingIndicatorView.hide();

//
//        Picasso.with(context)
//                .load(baseClass.imageBaseUrl + alImage.get(i).getImageUrl())
//                .placeholder(R.drawable.round_white)
//                .into(viewHolder.imgThumbnail);

//        Glide.with(context)
//                .load(baseClass.imageBaseUrl + alImage.get(i).getImageUrl())
//                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                .dontTransform()
//                .placeholder(R.drawable.cover_home)
//
//                .into(viewHolder.imgThumbnail);



    }

    @Override
    public int getItemCount() {
        return alImage.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        public RoundedImageView imgThumbnail;
        public TextView tvName;


        public ViewHolder(View itemView) {
            super(itemView);
            imgThumbnail = (RoundedImageView) itemView.findViewById(R.id.main_image);
            tvName = (TextView) itemView.findViewById(R.id.service_categories_name);

        }

        public void setClickListener(ItemClickListener itemClickListener) {
//            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
//            clickListener.onClick(view, getPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
//            clickListener.onClick(view, getPosition(), true);
            return true;
        }
    }

}