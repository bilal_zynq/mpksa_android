package com.schopfen.mpksaproject.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.schopfen.mpksaproject.HelperClasses.BaseClass;
import com.schopfen.mpksaproject.HelperClasses.HomePagerData;
import com.schopfen.mpksaproject.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HomePagerAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<HomePagerData> homePagerDataArrayList;
    private LayoutInflater layoutInflater;

    public HomePagerAdapter(Context context, ArrayList<HomePagerData> homePagerDataArrayList) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) this.context.getSystemService(this.context.LAYOUT_INFLATER_SERVICE);
        this.homePagerDataArrayList = homePagerDataArrayList;
    }

    @Override
    public int getCount() {
        return homePagerDataArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = this.layoutInflater.inflate(R.layout.home_pager_item, container, false);
        RoundedImageView mainImage = (RoundedImageView) view.findViewById(R.id.main_image);
        TextView serviceName = (TextView) view.findViewById(R.id.serviceName);
        TextView serviceType = (TextView) view.findViewById(R.id.serviceType);

        Picasso.get().load(BaseClass.imageBaseUrl + homePagerDataArrayList.get(position).getPackageImage()).into(mainImage);
        serviceName.setText(homePagerDataArrayList.get(position).getTitle());
//        serviceType

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}