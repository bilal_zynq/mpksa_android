package com.schopfen.mpksaproject.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.makeramen.roundedimageview.RoundedImageView;
import com.schopfen.mpksaproject.HelperClasses.BaseClass;
import com.schopfen.mpksaproject.HelperClasses.HomeGridItem;
import com.schopfen.mpksaproject.R;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class HomeGridAdapter extends ArrayAdapter<HomeGridItem> {

    Context mContext;
    private ArrayList<HomeGridItem> homeGridItemArrayList;
    RoundedImageView categoryImage;
    TextView categoryName;


    public HomeGridAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            grid = inflater.inflate(R.layout.home_grid_item, null);
        } else {
            grid = (View) convertView;
        }

        categoryImage = (RoundedImageView) grid.findViewById(R.id.main_image);
        categoryName = (TextView)grid.findViewById(R.id.service_categories_name);
        Picasso.get().load(BaseClass.imageBaseUrl + homeGridItemArrayList.get(position).getImage()).into(categoryImage);
        categoryName.setText(homeGridItemArrayList.get(position).getTitle());

        return grid;
    }

    @Nullable
    @Override
    public HomeGridItem getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(@Nullable HomeGridItem item) {
        return super.getPosition(item);
    }

    public HomeGridAdapter(Context mContext, ArrayList<HomeGridItem> homeGridItemArrayList) {
        super(mContext, R.layout.home_grid_item, homeGridItemArrayList);
        this.mContext = mContext;
        this.homeGridItemArrayList = homeGridItemArrayList;
    }

}
