package com.schopfen.mpksaproject.HelperClasses;

public class HomeGridItem {
    String CategoryID;
    String CategoryPrice;
    String Image;
    String ParentID;
    String Title;

    public HomeGridItem(String categoryID, String categoryPrice, String image, String parentID, String title) {
        CategoryID = categoryID;
        CategoryPrice = categoryPrice;
        Image = image;
        ParentID = parentID;
        Title = title;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String categoryID) {
        CategoryID = categoryID;
    }

    public String getCategoryPrice() {
        return CategoryPrice;
    }

    public void setCategoryPrice(String categoryPrice) {
        CategoryPrice = categoryPrice;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getParentID() {
        return ParentID;
    }

    public void setParentID(String parentID) {
        ParentID = parentID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
