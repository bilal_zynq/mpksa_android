package com.schopfen.mpksaproject.HelperClasses;

public class HomePagerData {

    String Description;
    String PackageID;
    String PackageImage;
    String PackageTextID;
    String Title;
    String VisitPerYear;

    public HomePagerData(String description, String packageID, String packageImage, String packageTextID, String title, String visitPerYear) {
        Description = description;
        PackageID = packageID;
        PackageImage = packageImage;
        PackageTextID = packageTextID;
        Title = title;
        VisitPerYear = visitPerYear;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getPackageID() {
        return PackageID;
    }

    public void setPackageID(String packageID) {
        PackageID = packageID;
    }

    public String getPackageImage() {
        return PackageImage;
    }

    public void setPackageImage(String packageImage) {
        PackageImage = packageImage;
    }

    public String getPackageTextID() {
        return PackageTextID;
    }

    public void setPackageTextID(String packageTextID) {
        PackageTextID = packageTextID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getVisitPerYear() {
        return VisitPerYear;
    }

    public void setVisitPerYear(String visitPerYear) {
        VisitPerYear = visitPerYear;
    }
}
