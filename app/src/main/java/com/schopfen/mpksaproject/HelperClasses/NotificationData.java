package com.schopfen.mpksaproject.HelperClasses;

public class NotificationData {

    String name, image, orderNumber, date;

    public NotificationData(String name, String image, String orderNumber, String date) {
        this.name = name;
        this.image = image;
        this.orderNumber = orderNumber;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
