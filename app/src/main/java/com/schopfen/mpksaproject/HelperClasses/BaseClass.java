package com.schopfen.mpksaproject.HelperClasses;

import android.content.Context;
import android.graphics.Color;
import android.text.format.DateFormat;
import android.util.Log;

import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;
import com.leo.simplearcloader.SimpleArcLoader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Pattern;


public class BaseClass {

    public Context mContext;

    public BaseClass(Context context) {
        mContext = context;
    }


    public String baseUrl = "https://bsap.mp-ksa.com/api/";
    public static String imageBaseUrl = "http://bsap.mp-ksa.com/";

    public String generateTokenByApi = baseUrl + "generateTokenByApi";
    public String sendOTP = baseUrl + "sendOTP";
    public String verifyOTP = baseUrl + "verifyOTP";
    public String cities = baseUrl + "cities";
    public String updateProfile = baseUrl + "updateProfile";
    public String categories = baseUrl + "categories";
    public String packages = baseUrl + "packages";

    public String getUserDetail = baseUrl + "getUserDetail" ;

    public static SimpleArcDialog showProgressDialog(Context context) {

        SimpleArcDialog mDialog;
        mDialog = new SimpleArcDialog(context);
        int[] colorss = new int[]{Color.parseColor("#222222"), Color.parseColor("#D9D9D9")};
        ArcConfiguration configuration = new ArcConfiguration(context);
        configuration.setLoaderStyle(SimpleArcLoader.STYLE.COMPLETE_ARC);
        configuration.setColors(colorss);
        configuration.setText("");
        mDialog.setConfiguration(configuration);
        mDialog.setCancelable(false);
        mDialog.show();
        return mDialog;
    }

    public static String TmeStamp() {
        long timeInMilliseconds, timeInMilliseconds1;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        //Here you say to java the initial timezone. This is the secret
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        int seconds = timeZone.getOffset(Calendar.ZONE_OFFSET) / 1000;
        //Will print in UTC
        Date mDate = null;
        try {
            mDate = sdf.parse(sdf.format(calendar.getTime()));
            timeInMilliseconds = mDate.getTime();


        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println(sdf.format(calendar.getTime()));
        //Here you set to your timezone
        sdf.setTimeZone(TimeZone.getDefault());
        //Will print on your default Timezone
        TimeZone timeZone1 = TimeZone.getDefault();
        int seconds1 = timeZone1.getOffset(Calendar.ZONE_OFFSET) / 1000;
        System.out.println(sdf.format(calendar.getTime()));

        try {
            Date mDate1 = sdf.parse(sdf.format(calendar.getTime()));
            timeInMilliseconds1 = mDate1.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }


        long differ = seconds1 - seconds;
        calendar.setTime(mDate);
        calendar.add(Calendar.SECOND, (int) differ);

        Log.e("testingGmt", calendar.getTime() + "");
        Long tsLong = System.currentTimeMillis() / 1000;
        Long l = calendar.getTimeInMillis() / 1000;
        String ts = l.toString();
        Log.e("testingGmt", ts + "");

        return ts;
    }

    public static String timeStampToDate(long timestamp) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();

        return date;
    }

    public static String getDateTimeFromTimeStampUTC(long timestamp) {
        try {
            long timeInMilliseconds;

            TimeZone timeZone = TimeZone.getTimeZone("UTC");
            Calendar calendar = Calendar.getInstance(timeZone);
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, timeZone.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MMMM.yyyy");
            int seconds = timeZone.getOffset(Calendar.ZONE_OFFSET) / 1000;
            Date mDate = null;
            try {
                mDate = sdf.parse(sdf.format(calendar.getTime()));
                timeInMilliseconds = mDate.getTime();
                Log.e("testingGmt", timeInMilliseconds + "");

            } catch (ParseException e) {
                e.printStackTrace();
            }

            TimeZone timeZone1 = TimeZone.getDefault();
            int seconds1 = timeZone1.getOffset(Calendar.ZONE_OFFSET) / 1000;
            int difference = seconds1 - seconds;
            calendar.setTime(mDate);
            calendar.add(Calendar.SECOND, (int) -difference);
            return sdf.format(calendar.getTime());
        } catch (Exception e) {
        }
        return "";
    }

    public static Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9+._%-+]{1,256}" +
                    "@" +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                    "(" +
                    "." +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                    ")+"
    );

    public static boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }


}
