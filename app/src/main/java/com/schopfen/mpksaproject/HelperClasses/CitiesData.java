package com.schopfen.mpksaproject.HelperClasses;

public class CitiesData {
    String CityID;
    String CreatedAt;
    String CreatedBy;
    String DistrictID;
    String SystemLanguageID;
    String Title;
    String SortOrder;

    public CitiesData(String cityID, String createdAt, String createdBy, String districtID, String systemLanguageID, String title, String sortOrder) {
        CityID = cityID;
        CreatedAt = createdAt;
        CreatedBy = createdBy;
        DistrictID = districtID;
        SystemLanguageID = systemLanguageID;
        Title = title;
        SortOrder = sortOrder;
    }

    public String getCityID() {
        return CityID;
    }

    public void setCityID(String cityID) {
        CityID = cityID;
    }

    public String getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        CreatedAt = createdAt;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getDistrictID() {
        return DistrictID;
    }

    public void setDistrictID(String districtID) {
        DistrictID = districtID;
    }

    public String getSystemLanguageID() {
        return SystemLanguageID;
    }

    public void setSystemLanguageID(String systemLanguageID) {
        SystemLanguageID = systemLanguageID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getSortOrder() {
        return SortOrder;
    }

    public void setSortOrder(String sortOrder) {
        SortOrder = sortOrder;
    }
}
