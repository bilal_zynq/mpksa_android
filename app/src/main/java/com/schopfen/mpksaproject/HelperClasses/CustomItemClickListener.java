package com.schopfen.mpksaproject.HelperClasses;

import android.view.View;

public interface CustomItemClickListener {
    public void onItemClick(View v, int position);
}