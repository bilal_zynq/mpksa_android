package com.schopfen.mpksaproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.schopfen.mpksaproject.HelperClasses.BaseClass;
import com.schopfen.mpksaproject.HelperClasses.CustomItemClickListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity implements View.OnClickListener {

    SharedPreferences sharedpreferences;
    SharedPreferences.Editor mEditor;
    public static final String MyPREFERENCES = "MyPrefs";
    Button register;
    EditText phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        mEditor = sharedpreferences.edit();

        register = findViewById(R.id.btn_register);
        register.setOnClickListener(this);
        phone = (EditText) findViewById(R.id.registration_phoneno);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Register.this, SelectLanguage.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_register:
//
//                BaseClass baseClass1 = new BaseClass(Register.this);
//                RequestQueue queue1 = Volley.newRequestQueue(Register.this);
//                String url1 = baseClass1.sendOTP;
////
//                StringRequest strReq = new StringRequest(Request.Method.POST,
//                        url1, new Response.Listener<String>() {
//
//                    @Override
//                    public void onResponse(String response) {
//                        Log.e("Response", response);
//
//                        try {
//                            JSONObject jsonObject = new JSONObject(response.toString());
//                            boolean status = jsonObject.getBoolean("status");
//
//                            if (status){
////                        simpleArcDialog.dismiss();
////                        JSONObject userInfo = jsonObject.getJSONObject("user_info");
//                                String is_new_user = jsonObject.getString("is_new_user");
//                                String message = jsonObject.getString("message");
//                                Toast.makeText(Register.this, message, Toast.LENGTH_SHORT).show();
//                                Intent intent = new Intent(Register.this, Verify.class);
//                                startActivity(intent);
//                                finish();
//                            } else{
////                        String message = jsonObject.getString("message");
////                        simpleArcDialog.dismiss();
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//
//                            Log.e("Response", e.toString());
////                    simpleArcDialog.dismiss();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        VolleyLog.e("Response", "Error: " + error.getMessage());
//                        //Send message when something goes wrong
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
//                            }
//                        });
//                    }
//                }) {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<>();
//                        params.put("Mobile",phone.getText().toString());
//                        mEditor.putString("Mobile", phone.getText().toString()).commit();
//                        return params;
//                    }
//
//                    @Override
//                    public Map<String, String> getHeaders() throws AuthFailureError {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("Verifytoken", sharedpreferences.getString("ApiToken", " "));
//                        Log.e("header", params.toString());
//                        return params;
//                    }
//                };
//                int socketTimeout = 30000;//30 seconds - change to what you want
//                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//                strReq.setRetryPolicy(policy);
//                queue1.add(strReq);
              codeGeneration(this,true,sharedpreferences,mEditor,phone,"");

                break;

        }


    }


    public void codeGeneration(final Context context, final boolean type, final SharedPreferences sharedPreferences, final SharedPreferences.Editor editor, final EditText verifyCode, final String test) {

        BaseClass baseClass1 = new BaseClass(context);
        RequestQueue queue1 = Volley.newRequestQueue(context);
        String url1 = baseClass1.sendOTP;
        Log.e("Response", url1);

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url1, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("Response", response);

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    boolean status = jsonObject.getBoolean("status");

                    if (status) {
//                        simpleArcDialog.dismiss();
//                        JSONObject userInfo = jsonObject.getJSONObject("user_info");
                        String is_new_user = jsonObject.getString("is_new_user");
                        String message = jsonObject.getString("message");

                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();


                        if (type) {
                            Intent intent = new Intent(context, Verify.class);
                            startActivity(intent);
                            finish();
                        } else {

                            Toast.makeText(context, "Code Resent", Toast.LENGTH_SHORT).show();

                        }
//                        String message = jsonObject.getString("message");
//                        simpleArcDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.e("Response", e.toString());
//                    simpleArcDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Response", "Error: " + error.getMessage());
                //Send message when something goes wrong
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (type){
                    params.put("Mobile", phone.getText().toString());
                    mEditor.putString("Mobile", phone.getText().toString()).commit();
                    Log.e("testing","true");
                }else {
                    params.put("Mobile", test);
                    Log.e("testing","false "+test);


                }
                Log.e("testing",params.toString());


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Verifytoken", sharedPreferences.getString("ApiToken", " "));
                Log.e("header", params.toString());
                return params;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);
        queue1.add(strReq);

    }
}



