package com.schopfen.mpksaproject;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.irozon.justbar.BarItem;
import com.irozon.justbar.JustBar;
import com.irozon.justbar.interfaces.OnBarItemClickListener;
import com.schopfen.mpksaproject.Fragments.Home;
import com.schopfen.mpksaproject.Fragments.More;
import com.schopfen.mpksaproject.Fragments.Notifications;
import com.schopfen.mpksaproject.Fragments.Orders;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int MULTIPLE_PERMISSIONS = 10;
    public String[] permissions = new String[]{
            Manifest.permission.INTERNET,
    };
    JustBar justBar;
    Home homeFragment;
    More moreFragment;
    Orders orderFragment;
    Notifications notificationFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Setting Views
        setViews();

        justBar.setOnBarItemClickListener(new OnBarItemClickListener() {
            @Override
            public void onBarItemClick(BarItem barItem, int position) {
                switch (position) {
                    case 0:

                        setFragment(homeFragment);

                        break;

                    case 1:

                        setFragment(orderFragment);

                        break;

                    case 2:
                        setFragment(notificationFragment);


                        break;
                    case 3:
                        setFragment(moreFragment);

                        break;


                }
            }
        });


        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Home homeFragment = new Home();
        fragmentTransaction.add(R.id.fragmentContainer, homeFragment);
        fragmentTransaction.commit();
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, fragment);
        fragmentTransaction.commit();

    }

    public void setViews() {

        checkPermissions();
        homeFragment = new Home();
        moreFragment = new More();
        notificationFragment = new Notifications();
        orderFragment = new Orders();

        justBar = (JustBar) findViewById(R.id.bottomBar);
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(MainActivity.this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permissions granted.
                } else {
                    // no permissions granted.
                }
                return;
            }
        }
    }

}
