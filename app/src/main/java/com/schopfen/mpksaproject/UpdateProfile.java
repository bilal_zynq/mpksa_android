package com.schopfen.mpksaproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.appersiano.progressimage.ProgressImage;
import com.leo.simplearcloader.SimpleArcDialog;
import com.schopfen.mpksaproject.HelperClasses.BaseClass;
import com.schopfen.mpksaproject.HelperClasses.CitiesData;
import com.schopfen.mpksaproject.HelperClasses.CustomItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.schopfen.mpksaproject.WelcomeAfterVerification.MyPREFERENCES;

public class UpdateProfile extends AppCompatActivity implements View.OnClickListener {
    EditText name, email, city, phone;
    Button btn_update;


    SharedPreferences sharedpreferences;
    SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myaccount);
        view();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        mEditor = sharedpreferences.edit();




        BaseClass baseClass1 = new BaseClass(UpdateProfile.this);
        RequestQueue queue1 = Volley.newRequestQueue(UpdateProfile.this);
        String url1 = baseClass1.getUserDetail + "?UserID=" + sharedpreferences.getString("UserID", "");
        Log.e("111", "" + url1);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hobbyGroupsDataList.clear();
                        Log.e("Response", response.toString());
//                        try {
//                            JSONObject mainObject = new JSONObject(response.toString());
//                            boolean status = mainObject.getBoolean("status");
//
//                            if(status){
////                                simpleArcDialog.dismiss();
//
//                                JSONArray jsonArray = mainObject.getJSONArray("cities");
//                                for (int i=0; i<jsonArray.length();i++) {
//                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
//
//                                    String CityID = jsonObject.getString("CityID");
//                                    String CreatedAt = jsonObject.getString("CreatedAt");
//                                    String CreatedBy = jsonObject.getString("CreatedBy");
//                                    String DistrictID = jsonObject.getString("DistrictID");
//                                    String SystemLanguageID = jsonObject.getString("SystemLanguageID");
//                                    String Title = jsonObject.getString("Title");
//                                    String SortOrder = jsonObject.getString("SortOrder");
//                                    Cities.add(Title);
////                                    ma[i]=title;
//                                    citiesDataArrayList.add(new CitiesData(CityID, CreatedAt, CreatedBy, DistrictID, SystemLanguageID, Title, SortOrder));
////                                    for (int j = 0; j  homeGridDataArrayList.size(); j++) {
////                                        mainCategories.add(homeGridDataArrayList.get(j).getTitle());
////                                    }
//                                }
////                                simpleArcDialog.dismiss();
//                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(UpdateProfile.this,
//                                        android.R.layout.simple_list_item_1, Cities);
//                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
//                                city.setAdapter(adapter);
//                            }
//                        } catch (JSONException e) {
//                            Log.e("Response", e.toString());
//                            e.printStackTrace();
////                                    simpleArcDialog.dismiss();
//                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                        simpleArcDialog.dismiss();
                error.printStackTrace();
                Log.e("Response", error.toString());
            }


        }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Verifytoken", sharedpreferences.getString("ApiToken", " "));

                return params;
            }
        };
        int socketTimeout1 = 30000;
        RetryPolicy policy1 = new DefaultRetryPolicy(socketTimeout1, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy1);
        queue1.add(jsonObjectRequest);






    }











    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.myaccount_btn_update :



        }
    }






    public void view() {


        name = findViewById(R.id.myaccount_fullname);
        email = findViewById(R.id.myaccount_email);
        city = findViewById(R.id.myaccount_city);
        phone = findViewById(R.id.myaccount_phone);
        btn_update = findViewById(R.id.myaccount_btn_update);
        btn_update.setOnClickListener(this);

    }






}






