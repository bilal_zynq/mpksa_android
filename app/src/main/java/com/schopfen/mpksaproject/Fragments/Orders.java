package com.schopfen.mpksaproject.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schopfen.mpksaproject.Adapters.OrdersAdapter;
import com.schopfen.mpksaproject.HelperClasses.OrderData;
import com.schopfen.mpksaproject.R;

import java.util.ArrayList;


public class Orders extends Fragment implements View.OnClickListener {
    private static final String ARG_TEXT = "arg_text";
    private static final String ARG_COLOR = "arg_color";
    private String mText;
    private int mColor;
    private View mContent;
    RecyclerView orders;
    RecyclerView.LayoutManager mLayoutManager;
    OrdersAdapter ordersAdapter;

    public static Fragment newInstance(String text) {
        Fragment frag = new Orders();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, text);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.myorders, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        orders = view.findViewById(R.id.orders_recycler);
        ArrayList<OrderData> getOrderData = dataSource();
        ordersAdapter = new OrdersAdapter(getActivity(), getOrderData);
        mLayoutManager = new LinearLayoutManager(getActivity());
        orders.setLayoutManager(mLayoutManager);
        orders.setAdapter(ordersAdapter);
        ordersAdapter.notifyDataSetChanged();
    }

    private ArrayList<OrderData> dataSource() {
        ArrayList<OrderData> data = new ArrayList<OrderData>();
        data.add(new OrderData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new OrderData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new OrderData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new OrderData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new OrderData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new OrderData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new OrderData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new OrderData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new OrderData("test", "Air Conditioning", "Maintenance Service", ""));

        return data;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(ARG_TEXT, mText);
        outState.putInt(ARG_COLOR, mColor);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {


        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        // TODO Auto-generated method stub
        super.onAttachFragment(fragment);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStart() {
        super.onStart();


    }


}

