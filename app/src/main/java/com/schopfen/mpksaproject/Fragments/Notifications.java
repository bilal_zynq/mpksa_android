package com.schopfen.mpksaproject.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schopfen.mpksaproject.Adapters.NotificationsAdapter;
import com.schopfen.mpksaproject.Adapters.OrdersAdapter;
import com.schopfen.mpksaproject.HelperClasses.NotificationData;
import com.schopfen.mpksaproject.HelperClasses.OrderData;
import com.schopfen.mpksaproject.R;

import java.util.ArrayList;


public class Notifications extends Fragment implements View.OnClickListener {
    private static final String ARG_TEXT = "arg_text";
    private static final String ARG_COLOR = "arg_color";
    private String mText;
    private int mColor;
    private View mContent;
    RecyclerView notifications;
    RecyclerView.LayoutManager mLayoutManager;
    NotificationsAdapter notificationsAdapter;

    public static Fragment newInstance(String text) {
        Fragment frag = new Notifications();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, text);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.notification, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        notifications = view.findViewById(R.id.notification_recycler);
        ArrayList<NotificationData> getOrderData = dataSource();
        notificationsAdapter = new NotificationsAdapter(getActivity(), getOrderData);
        mLayoutManager = new LinearLayoutManager(getActivity());
        notifications.setLayoutManager(mLayoutManager);
        notifications.setAdapter(notificationsAdapter);
        notificationsAdapter.notifyDataSetChanged();

    }


    private ArrayList<NotificationData> dataSource() {
        ArrayList<NotificationData> data = new ArrayList<NotificationData>();
        data.add(new NotificationData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new NotificationData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new NotificationData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new NotificationData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new NotificationData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new NotificationData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new NotificationData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new NotificationData("test", "Air Conditioning", "Maintenance Service", ""));
        data.add(new NotificationData("test", "Air Conditioning", "Maintenance Service", ""));

        return data;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(ARG_TEXT, mText);
        outState.putInt(ARG_COLOR, mColor);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {


        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        // TODO Auto-generated method stub
        super.onAttachFragment(fragment);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStart() {
        super.onStart();


    }


}

