package com.schopfen.mpksaproject.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bikomobile.circleindicatorpager.CircleIndicatorPager;
import com.schopfen.mpksaproject.Adapters.HomeGridAdapter;
import com.schopfen.mpksaproject.Adapters.HomePagerAdapter;
import com.schopfen.mpksaproject.HelperClasses.BaseClass;
import com.schopfen.mpksaproject.HelperClasses.HomeGridItem;
import com.schopfen.mpksaproject.HelperClasses.HomePagerData;
import com.schopfen.mpksaproject.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Home extends Fragment implements View.OnClickListener {
    private static final String ARG_TEXT = "arg_text";
    private static final String ARG_COLOR = "arg_color";
    private String mText;
    private int mColor;
    private View mContent;
    ViewPager viewPager;
    private HomePagerAdapter homePagerAdapter;
    private HomeGridAdapter homeGridAdapter;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor mEditor;
    public static final String MyPREFERENCES = "MyPrefs";
    ArrayList<HomeGridItem>homeGridItemArrayList = new ArrayList<>();
    ArrayList<HomePagerData>homePagerDataArrayList = new ArrayList<>();
    GridView servicesGrid;


    CircleIndicatorPager indicator;
    RecyclerView serviceCategories;
    RecyclerView.LayoutManager mLayoutManager;

    public static Fragment newInstance(String text) {
        Fragment frag = new Home();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, text);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        mEditor = sharedpreferences.edit();

        viewPager = (ViewPager) view.findViewById(R.id.pager);
        indicator = (CircleIndicatorPager) view.findViewById(R.id.tab_layout);
//        serviceCategories = (RecyclerView) view.findViewById(R.id.service_categories);
        servicesGrid = view.findViewById(R.id.servicesGrid);

        BaseClass baseClass1 = new BaseClass(getActivity());
        RequestQueue queue1 = Volley.newRequestQueue(getActivity());
        String url1 = baseClass1.categories + "?UserID=" + sharedpreferences.getString("UserID", "");
        Log.e("111", "" + url1);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hobbyGroupsDataList.clear();
                        Log.e("CategoriesResponse", response.toString());
                        try {
                            JSONObject mainObject = new JSONObject(response.toString());
                            boolean status = mainObject.getBoolean("status");

                            if(status){
//                                simpleArcDialog.dismiss();

                                JSONArray jsonArray = mainObject.getJSONArray("categories");
                                for (int i=0; i<jsonArray.length();i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    String CategoryID = jsonObject.getString("CategoryID");
                                    String CategoryPrice = jsonObject.getString("CategoryPrice");
                                    String Image = jsonObject.getString("Image");
                                    String ParentID = jsonObject.getString("ParentID");
                                    String Title = jsonObject.getString("Title");

                                    homeGridItemArrayList.add(new HomeGridItem(CategoryID, CategoryPrice, Image, ParentID, Title));
                                }

                                HomeGridAdapter adapter = new HomeGridAdapter(getActivity(), homeGridItemArrayList);
                                servicesGrid.setAdapter(adapter);
                                servicesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                        Log.e("GridItemClick", " " + i);
                                    }
                                });
//                                simpleArcDialog.dismiss();
                            }
                        } catch (JSONException e) {
                            Log.e("Response", e.toString());
                            e.printStackTrace();
//                                    simpleArcDialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                        simpleArcDialog.dismiss();
                error.printStackTrace();
                Log.e("Response", error.toString());
            }


        }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Verifytoken", sharedpreferences.getString("ApiToken", " "));

                return params;
            }
        };
        int socketTimeout1 = 30000;
        RetryPolicy policy1 = new DefaultRetryPolicy(socketTimeout1, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy1);
        queue1.add(jsonObjectRequest);



        BaseClass baseClass = new BaseClass(getActivity());
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = baseClass.packages + "?UserID=" + sharedpreferences.getString("UserID", "");
        Log.e("111", "" + url1);

        JsonObjectRequest jsonObjectRequest1 = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hobbyGroupsDataList.clear();
                        Log.e("PackagesResponse", response.toString());
                        try {
                            JSONObject mainObject = new JSONObject(response.toString());
                            boolean status = mainObject.getBoolean("status");

                            if(status){
//                                simpleArcDialog.dismiss();

                                JSONArray jsonArray = mainObject.getJSONArray("packages");
                                for (int i=0; i<jsonArray.length();i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    String Description = jsonObject.getString("Description");
                                    String PackageID = jsonObject.getString("PackageID");
                                    String PackageImage = jsonObject.getString("PackageImage");
                                    String PackageTextID = jsonObject.getString("PackageTextID");
                                    String Title = jsonObject.getString("Title");
                                    String VisitPerYear = jsonObject.getString("VisitPerYear");

                                    homePagerDataArrayList.add(new HomePagerData(Description, PackageID, PackageImage, PackageTextID, Title, VisitPerYear));
                                }

                                    homePagerAdapter = new HomePagerAdapter(getActivity(), homePagerDataArrayList);
                                    viewPager.setClipToPadding(false);
                                    viewPager.setOffscreenPageLimit(2);
                                    viewPager.setPadding(60, 0, 60, 0);
                                    viewPager.setPageMargin(20);
                                    viewPager.setAdapter(homePagerAdapter);
                                    indicator.setViewPager(viewPager);

//                                simpleArcDialog.dismiss();
                            }
                        } catch (JSONException e) {
                            Log.e("Response", e.toString());
                            e.printStackTrace();
//                                    simpleArcDialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                        simpleArcDialog.dismiss();
                error.printStackTrace();
                Log.e("Response", error.toString());
            }

        }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Verifytoken", sharedpreferences.getString("ApiToken", " "));

                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest1.setRetryPolicy(policy);
        queue.add(jsonObjectRequest1);


//        List<HomePagerData> getData = dataSource();
//

//        ArrayList<HomeGridItem> getGridData = dataGridSource();
//        homeGridAdapter = new HomeGridAdapter(getActivity(), getGridData);
//        mLayoutManager = new GridLayoutManager(getActivity(), 3);
//        serviceCategories.setLayoutManager(mLayoutManager);
//        serviceCategories.setAdapter(homeGridAdapter);
//        serviceCategories.addItemDecoration(new EqualSpacingItemDecoration(18));
//        serviceCategories.setNestedScrollingEnabled(false);
//        serviceCategories.setFocusable(false);
//        homeGridAdapter.notifyDataSetChanged();
//        homePagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {

    }

//    private List<HomePagerData> dataSource() {
//        List<HomePagerData> data = new ArrayList<HomePagerData>();
//        data.add(new HomePagerData("test", "Air Conditioning", "Maintenance Service"));
//        data.add(new HomePagerData("test", "Air Conditioning", "Maintenance Service"));
//        data.add(new HomePagerData("test", "Air Conditioning", "Maintenance Service"));
//        data.add(new HomePagerData("test", "Air Conditioning", "Maintenance Service"));
//        data.add(new HomePagerData("test", "Air Conditioning", "Maintenance Service"));
//        return data;
//    }

//    private ArrayList<HomeGridItem> dataGridSource() {
//        ArrayList<HomeGridItem> data = new ArrayList<HomeGridItem>();
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//        data.add(new HomeGridItem("test", "Plumbing"));
//
//        return data;
//    }

}

