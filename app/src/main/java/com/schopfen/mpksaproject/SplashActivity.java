package com.schopfen.mpksaproject;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.appersiano.progressimage.ProgressImage;
import com.leo.simplearcloader.SimpleArcDialog;
import com.schopfen.mpksaproject.HelperClasses.BaseClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends AppCompatActivity {
    protected int _splashTime = 3 * 1000;
    ProgressImage progressImage;
    SimpleArcDialog simpleArcDialog;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor mEditor;
    public static final String MyPREFERENCES = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        progressImage = findViewById(R.id.vlImage);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        mEditor = sharedpreferences.edit();

        progressImage.setProgress(100, true);

//        simpleArcDialog = BaseClass.showProgressDialog(SplashActivity.this);
//        simpleArcDialog.show();
        BaseClass baseClass1 = new BaseClass(SplashActivity.this);
        RequestQueue queue1 = Volley.newRequestQueue(SplashActivity.this);
        String url1 = baseClass1.generateTokenByApi;

        JSONObject jsonObject1=new JSONObject();
        try {
            jsonObject1.put("","");
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        request a json object response
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url1, jsonObject1, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Response", response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    boolean status = jsonObject.getBoolean("status");

                    if (status){
//                        simpleArcDialog.dismiss();
//                        JSONObject userInfo = jsonObject.getJSONObject("user_info");
                        String token = jsonObject.getString("token");
                        mEditor.putString("ApiToken", token).commit();
                        Intent i = new Intent();
                        i.setClass(SplashActivity.this, SelectLanguage.class);
                        startActivity(i);
                        finish();

                    } else{
//                        String message = jsonObject.getString("message");
//                        simpleArcDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("Response", e.toString());
//                    simpleArcDialog.dismiss();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //handle the error
//                simpleArcDialog.dismiss();
                error.printStackTrace();
                Log.e("ErrorResponse", error.toString());

            }
        }) {    //this is the part, that adds the header to the request
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Newtoken", "true");
                return params;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);
        queue1.add(jsonObjReq);

//        Handler handler = new Handler();
//        Runnable r = new Runnable() {
//
//            public void run() {
//
//                Intent i = new Intent();
//                i.setClass(SplashActivity.this, SelectLanguage.class);
//                startActivity(i);
//                finish();
//
//            }
//        };
//        handler.postDelayed(r, _splashTime);

        @SuppressWarnings("serial")
         class ServerError extends VolleyError {
            public ServerError(NetworkResponse networkResponse){
                super(networkResponse);
        }
        public ServerError()
        {
            super();
        }

    }
}}


