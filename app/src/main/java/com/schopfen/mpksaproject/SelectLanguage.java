package com.schopfen.mpksaproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SelectLanguage extends AppCompatActivity implements View.OnClickListener {
    Button btEnglish, btArabic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);

        btEnglish = findViewById(R.id.btn_english_lang);
        btArabic = findViewById(R.id.btn_arabia_lang);

        btEnglish.setOnClickListener(this);
        btArabic.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {

            case R.id.btn_english_lang:
                intent = new Intent(SelectLanguage.this, Register.class);
                startActivity(intent);
                finish();
                break;

            case R.id.btn_arabia_lang:
                intent = new Intent(SelectLanguage.this, Register.class);
                startActivity(intent);
                finish();
                break;
        }

    }
}
