package com.schopfen.mpksaproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.schopfen.mpksaproject.HelperClasses.BaseClass;
import com.schopfen.mpksaproject.HelperClasses.CitiesData;
import com.schopfen.mpksaproject.HelperClasses.DrawableClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WelcomeAfterVerification extends AppCompatActivity implements View.OnClickListener {

    Button update;
    LinearLayout indivisualView, companyView;
    ImageView indivisualDrawable, companyDrawable;
    TextView indivisualText, companyText;
    EditText fullName, email;
    AutoCompleteTextView city;

    SharedPreferences sharedpreferences;
    SharedPreferences.Editor mEditor;
    public static final String MyPREFERENCES = "MyPrefs";
    ArrayList<String>Cities = new ArrayList<>();
    ArrayList<CitiesData> citiesDataArrayList = new ArrayList<>();
    String CityID;
    String UserType = "indivisual";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_after_verification);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        mEditor = sharedpreferences.edit();

        view();
        update = findViewById(R.id.welcome_btn_update);
        update.setOnClickListener(this);
        indivisualDrawable.setOnClickListener(this);
        companyDrawable.setOnClickListener(this);


                BaseClass baseClass1 = new BaseClass(WelcomeAfterVerification.this);
                RequestQueue queue1 = Volley.newRequestQueue(WelcomeAfterVerification.this);
                String url1 = baseClass1.cities + "?UserID=" + sharedpreferences.getString("UserID", "");
                Log.e("111", "" + url1);

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url1,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
//                        hobbyGroupsDataList.clear();
                                Log.e("CitiesResponse", response.toString());
                                try {
                                JSONObject mainObject = new JSONObject(response.toString());
                                boolean status = mainObject.getBoolean("status");

                            if(status){
//                                simpleArcDialog.dismiss();

                                JSONArray jsonArray = mainObject.getJSONArray("cities");
                                for (int i=0; i<jsonArray.length();i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    String CityID = jsonObject.getString("CityID");
                                    String CreatedAt = jsonObject.getString("CreatedAt");
                                    String CreatedBy = jsonObject.getString("CreatedBy");
                                    String DistrictID = jsonObject.getString("DistrictID");
                                    String SystemLanguageID = jsonObject.getString("SystemLanguageID");
                                    String Title = jsonObject.getString("Title");
                                    String SortOrder = jsonObject.getString("SortOrder");
                                    Cities.add(Title);
//                                    ma[i]=title;
                                    citiesDataArrayList.add(new CitiesData(CityID, CreatedAt, CreatedBy, DistrictID, SystemLanguageID, Title, SortOrder));
//                                    for (int j = 0; j  homeGridDataArrayList.size(); j++) {
//                                        mainCategories.add(homeGridDataArrayList.get(j).getTitle());
//                                    }
                                }
//                                simpleArcDialog.dismiss();
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(WelcomeAfterVerification.this,
                                        android.R.layout.simple_list_item_1, Cities);
                                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                                city.setAdapter(adapter);
                            }
                                } catch (JSONException e) {
                                    Log.e("Response", e.toString());
                                    e.printStackTrace();
//                                    simpleArcDialog.dismiss();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        simpleArcDialog.dismiss();
                        error.printStackTrace();
                        Log.e("Response", error.toString());
                    }


                }){
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Verifytoken", sharedpreferences.getString("ApiToken", " "));

                        return params;
                    }
                };
                int socketTimeout1 = 30000;
                RetryPolicy policy1 = new DefaultRetryPolicy(socketTimeout1, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                jsonObjectRequest.setRetryPolicy(policy1);
                queue1.add(jsonObjectRequest);

        city.setOnTouchListener(new DrawableClickListener.RightDrawableClickListener(city) {
            @Override
            public boolean onDrawableClick() {
                city.showDropDown();
                return true;
            }
        });

        city.setTextIsSelectable(true);
        city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                try {
                    CityID = citiesDataArrayList.get(position).getCityID();
                    mEditor.putString("CityID", CityID).commit();
                    mEditor.putString("City", citiesDataArrayList.get(position).getTitle()).commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.welcome_btn_update:

                BaseClass baseClass1 = new BaseClass(WelcomeAfterVerification.this);
                RequestQueue queue1 = Volley.newRequestQueue(WelcomeAfterVerification.this);
                String url1 = baseClass1.updateProfile;
//
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        url1, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("UpdateProfileResponse", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
//                            String message = jsonObject.getString("message");

                            if (status){
//                        simpleArcDialog.dismiss();
//                                JSONObject userInfo = jsonObject.getJSONObject("user_info");
//                                String AuthToken = userInfo.getString("AuthToken");
//                                mEditor.putString("ApiToken", AuthToken).commit();
//                                String Mobile = userInfo.getString("Mobile");
//                                String UserID = userInfo.getString("UserID");
//                                mEditor.putString("UserID", UserID).commit();

//                                Toast.makeText(Verify.this, message, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(WelcomeAfterVerification.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            } else{

//                                Toast.makeText(Verify.this, message, Toast.LENGTH_SHORT).show();
//                        simpleArcDialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.e("Response", e.toString());
//                    simpleArcDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Response", "Error: " + error.getMessage());
                        //Send message when something goes wrong
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                            }
                        });
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Email", email.getText().toString());
                        params.put("FullName", fullName.getText().toString());
                        params.put("CityID", CityID);
                        params.put("UserType ", UserType);
                        params.put("UserID", sharedpreferences.getString("UserID", ""));
                        Log.e("Params", params.toString());
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Verifytoken", sharedpreferences.getString("ApiToken", " "));
                        Log.e("header", params.toString());
                        return params;
                    }
                };
                int socketTimeout = 30000;//30 seconds - change to what you want
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                strReq.setRetryPolicy(policy);
                queue1.add(strReq);

                break;
            case R.id.indivisualDrawable:
                indivisualView.setBackground(getResources().getDrawable(R.drawable.white_background));
                indivisualDrawable.setImageResource(R.drawable.home_red);
                indivisualText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                companyView.setBackgroundColor(Color.TRANSPARENT);
                companyDrawable.setImageResource(R.drawable.company_white);
                companyText.setTextColor(getResources().getColor(R.color.white));
                UserType = "indivisual";
                Log.e("Indivisual", "Clicked");
                break;
            case R.id.companyDrawable:
                indivisualView.setBackgroundColor(Color.TRANSPARENT);
                indivisualDrawable.setImageResource(R.drawable.home_white);
                indivisualText.setTextColor(getResources().getColor(R.color.white));

                companyView.setBackground(getResources().getDrawable(R.drawable.white_background));
                companyDrawable.setImageResource(R.drawable.company_red);
                companyText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                Log.e("Company", "Clicked");
                UserType = "company";
                break;
        }
    }

    public void view(){
        indivisualView = (LinearLayout)findViewById(R.id.indivisualView);
        companyView = (LinearLayout)findViewById(R.id.companyView);
        indivisualDrawable = (ImageView) findViewById(R.id.indivisualDrawable);
        companyDrawable = (ImageView) findViewById(R.id.companyDrawable);
        indivisualText = (TextView) findViewById(R.id.indivisualText);
        companyText = (TextView) findViewById(R.id.companyText);
        fullName = findViewById(R.id.fullname);
        email = findViewById(R.id.email);
        city = findViewById(R.id.city);
    }
}
