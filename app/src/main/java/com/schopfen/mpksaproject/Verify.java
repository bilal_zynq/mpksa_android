package com.schopfen.mpksaproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.schopfen.mpksaproject.HelperClasses.BaseClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Verify extends AppCompatActivity implements View.OnClickListener {

    Button verify;
    TextView resend;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor mEditor;
    public static final String MyPREFERENCES = "MyPrefs";

    EditText verifyCode;

    Register register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);

        register = new Register();

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        mEditor = sharedpreferences.edit();

        verify = findViewById(R.id.btn_verify);
        verify.setOnClickListener(this);
        verifyCode = (EditText) findViewById(R.id.verify_code);
        resend = findViewById(R.id.btn_resend);
        resend.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_verify:


                BaseClass baseClass1 = new BaseClass(this);
                RequestQueue queue1 = Volley.newRequestQueue(this);
                String url1 = baseClass1.verifyOTP;
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        url1, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            boolean status = jsonObject.getBoolean("status");
                            String message = jsonObject.getString("message");

                            if (status) {
//                        simpleArcDialog.dismiss();
                                JSONObject userInfo = jsonObject.getJSONObject("user_info");
                                String AuthToken = userInfo.getString("AuthToken");
                                mEditor.putString("ApiToken", AuthToken).commit();
                                String Mobile = userInfo.getString("Mobile");
                                String UserID = userInfo.getString("UserID");

                                if (status) {

                                    Intent intent = new Intent(Verify.this, WelcomeAfterVerification.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(Verify.this, message, Toast.LENGTH_SHORT).show();

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.e("Response", e.toString());
//                    simpleArcDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Response", "Error: " + error.getMessage());

                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Mobile", sharedpreferences.getString("Mobile", ""));
                        params.put("Code", verifyCode.getText().toString());


                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Verifytoken", sharedpreferences.getString("ApiToken", " "));
                        Log.e("header", params.toString());
                        return params;
                    }
                };
                int socketTimeout = 30000;//30 seconds - change to what you want
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                strReq.setRetryPolicy(policy);
                queue1.add(strReq);


            case R.id.btn_resend:

                Log.e("header", sharedpreferences.getString("Mobile",""));

                register.codeGeneration(this, false, sharedpreferences, mEditor, null, sharedpreferences.getString("Mobile",""));


        }
    }


}


